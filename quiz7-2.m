R = 1000;
C = 0.01e-6;

w0 = 1/(R*C);

num = [w0];
den = [1, w0];

sys1 = tf(num, den);
bode(sys1);
