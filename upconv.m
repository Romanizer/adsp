function y = upconv(x, Ts, M)
% upconversion of signal x by M
% x = sampled signal
% Ts = sampling period of x
% M = upsampling factor (must be integer)
L = 42 * M;
 % L = one-sided sinc length
N = length(x);
 % N = samples in x
y = zeros(1, (N-1)*M+1);
 % make space for M-1 zeros between samples
y(1:M:end) = x;
 % upsampling: insert zeros
s = sinc([-L:L] / M);
y = conv(y, s, "same");
endfunction
