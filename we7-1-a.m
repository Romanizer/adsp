R = 10e3;
C = 11e-9;

w0 = 1/(sqrt(2)*R*C);

num = [w0^2];
den = [1, sqrt(2)*w0, w0^2];

sys = tf(num, den);
bode(sys);
