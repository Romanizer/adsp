clc;
clear all;
fs = 44100;
t = [0:1/fs:2];
f0 = 200;
x = 0;
# max harmonics
K = 10;
A = ones(1, K);
#A(1) = 1;
A = [1,0,1,0,1,0,1,0,1,0,1,0];

for i = [1:length(A)]
  x = x + A(i) * 1/(pi*pi*i*i) * cos(2*pi*i*f0*t);
endfor

sound(x, fs);

n = [1 : round(3 * fs/f0)]; % index range covering first 3 periods
plot(t(n), x(n));
xlabel("Time [s]");
