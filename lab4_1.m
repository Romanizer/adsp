fa = 44100;

fs = 700;
f0 = 500;
ta = [0:1/fs:1];
for k = 3:-1:-3
  fk = f0 + k*fs
  x = cos(2*pi*fk*ta);
  sound(upconv(x, 1/fs, fa/fs), fa);
endfor
