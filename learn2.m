R1 = 100;
R2 = 100;
R3 = 100;
C = 1e-6;

K = R1/R3;
w0 = 1/R2/C;

num = [1, -K * w0];
den = [1, w0];
sys = tf(num, den);
bode(sys);
