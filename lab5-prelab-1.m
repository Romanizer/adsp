R = 100, L = 1e-3, C = 10e-9;

A = [0, -1/L, 0; 1/C, 0, -1/C; 0, 1, -R/L];
B = [1/L; 0; 0];
C = [0, 0, R];
D = 0;

sys = ss(A, B, C, D);

#pzmap(sys);
bode(sys);
#step(sys);
