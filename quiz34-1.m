clc;
clear all;
Ts = 2*pi/(90*pi);
t = [0:Ts:3];

f0 = 200;
#x = cos(2*pi*f0*t);
x1 = cos(30*pi*t);
x2 = cos(60*pi*t);
x3 = cos(90*pi*t);

K = 200;
stem((0:K/(Ts*f0)-1), x2(1:K/(Ts*f0)));hold on;
stem((0:K/(Ts*f0)-1), x1(1:K/(Ts*f0)));hold on;
xlabel('n'), ylabel('x[n]'), grid on
