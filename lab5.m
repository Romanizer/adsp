R = 100, L = 1e-3, C = 10e-9;

num = [ R/L, 0 ];
den = [ 1, R/L, 1/(L*C) ];
sys = tf(num, den);
[mag, pha, w] = bode(sys);

figure(2);
subplot(2,1,1), semilogx(w, 20*log10(mag)), grid;
xlabel('Frequency [rad/s]'), ylabel('Magnitude Response [dB]');
hold on
plot(2*pi*x(:,1), 20*log10(x(:,2)))
legend({'model', 'data'})

subplot(2,1,2), semilogx(w, pha), grid;
xlabel('Frequency [rad/s]'), ylabel('Phase Response [deg]');
hold on
plot(2*pi*x(:,1), x(:,3))
