# 1mH no ringing
# 10mH little ringing
# 100mH lots ringing

R = 100, L = 1e-3, C = 10e-9;
A = [ -1/(R*C), -1/C; 1/L, 0 ];
B = [ 1/(R*C); 0];
C = [ 1, 0];
D = 0;
sys = ss(A,B,C,D);
[y, t, _] = step(sys)
