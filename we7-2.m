R = 100;
C = 1e-6;

w0 = 1;
num = [10];
den = [10*R^3*C^3, 31/2*R^2*C^2, 10*R*C, 5];

sys = tf(num, den);
bode(sys);
