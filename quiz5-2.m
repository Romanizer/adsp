R = 100, L = 1e-3, C = 10e-9;

num = [R*C, 1]; 
den = [L*C, R*C, 1];
f0 = 1/(2*pi*sqrt(L*C));
f0

t = [0:0.01/f0:100/f0];
fa = f0
fb = f0/100
fc = 100*f0

sys = tf(num, den);
#bode(sys)

x = sin(2*pi*fb*t);
lsim(sys, x, t);

#pzmap(sys);
#bode(sys);
#step(sys);
