
L = 1e-3;
C = 10e-9;

A = [ 0, -1/L; 1/C, 0 ];
B = [ 1/L; 0];
C = [ 0, 1];
D = 0;
sys = ss(A,B,C,D);
bode(sys)
return;
[y,t,_] = impulse(sys);
plot(t, y);
xlabel("Time [s]"), ylabel("Step response [V]");
