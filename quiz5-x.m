Rs = 68;
R = Inf, L = 1e-3, C = 10e-9;

# This TF is from a system consisting of
# RLC in parallel, as Vo
# In series with Rs

num = [1/Rs/C, 0];
den = [1, 1/R/C+1/Rs/C, 1/L/C];

sys = tf(num, den);
bode(sys)
