Fs = 400;
t = [0:1/Fs:0.1];

x = sin(200*pi*t) + 1/4*cos(400*pi*t) + 1/9*sin(600*pi*t) + 1/16*cos(800*pi*t) + 1/25*sin(1000*pi*t);

stem(t, x);
