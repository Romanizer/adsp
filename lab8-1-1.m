R1 = 74700;
C2 = 0.000000001; # 1nF
R3 = 2090000;
C4 = 0.0000000001; # 100pF

w0 = 1/sqrt(R1*C2*R3*C4)

num = [1];
den = [R1*C2*R3*C4, (R1*C4 + R3*C4), 1];

sys = tf(num, den);

figure(1);
bode(sys);
figure(2);
pzmap(sys);
figure(3);
step(sys);
