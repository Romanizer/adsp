R = 100, L = 1e-3, C = 10e-9;

f0 = 1/(2*pi*sqrt(L*C));
f0

t = [0:0.01/f0:10/f0];
fa = f0
fb = f0/2
fc = 2*f0

A = [0, 1/C; -1/L, -R/L];
B = [0; 1/L];
C = [0, -R];
D = 1;

sys = ss(A, B, C, D);

x = sin(2*pi*fc*t);
lsim(sys, x, t);

#pzmap(sys);
#bode(sys);
#step(sys);
