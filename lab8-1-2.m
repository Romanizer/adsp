R1 = 4390;
C1 = 0.00000001; # 10nF
R2 = 29900;
C2 = 0.00000001; # 10nF

w0 = 1/sqrt(R1*C2*R3*C4)

num = [1, 0, 0];
den = [1, 1/(R2*C1) + 1/(R2*C2), w0*w0];

sys = tf(num, den);

figure(1);
bode(sys);
figure(2);
pzmap(sys);
figure(3);
step(sys);
