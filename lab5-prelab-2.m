R = 100, L = 1e-3, C = 10e-9;

A = [-1/R, -1/C, 0; 1/L, 0, -1/L; 0, 1/C, 0];
B = [1/R; 0; 0];
C = [-1, 0, 0];
D = 1;

sys = ss(A, B, C, D);

#pzmap(sys);
bode(sys);
#step(sys);
