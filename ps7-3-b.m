C = 10e-9;

# first sallen-key
R = 4950;
K = 1.04;
num = [K];
den = [R*R*C*C, (3-K)*R*C, 1];

sys1 = tf(num, den);
figure(1);
bode(sys1);

# second sallen-key
R = 4700;
K = 1.364;
num = [K];
den = [R*R*C*C, (3-K)*R*C, 1];

sys2 = tf(num, den);

# third sallen-key
R = 4170;
K = 2.023;
num = [K];
den = [R*R*C*C, (3-K)*R*C, 1];

sys3 = tf(num, den);

syscomplete = sys1*sys2*sys3;
figure(2);
bode(syscomplete);
