
fc = 159;
R = 1000;
C = 1/(2*pi*R*fc)
num_lp = [1];
den = [R*C, 1];
sys_lp = tf(num_lp, den);

i = 1;

f0 = [fc/100, fc/10, fc, fc*10, fc*100];
for f = f0

    t = [0:0.05/f:3/f];
    x = sin(2*pi*f*t);
    figure(i++);
    lsim(sys_lp, x, t);

endfor
