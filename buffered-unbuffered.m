
w0=100000;

num = [w0];
den = [1, w0];

sys1 = tf(num, den);

#bode(sys1);

# buffered dual stage RC filter

sys2 = sys1 * sys1;

#figure(2);
#bode(sys2);

# unbuffered 2 stage RRCC filter
num = [w0*w0];
den = [1, 3*w0, w0*w0];

sys3 = tf(num, den);

bode(sys1, sys2, sys3);
