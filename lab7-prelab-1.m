% sys1 is a single stage LC Circuit
% sys2 is a dual stage LC Circuit, no buffer
% sys3 is a dual stage buffered LC Circuit

C = 1e-6;
L = 1e-3;
num = [1];
den1 = [L*C, 0, 1]
den2 = [L*L*C*C, 0, 2*L*C, 0, 1];
sys1 = tf(num, den1);
sys2 = tf(num, den2);
sys3 = sys1 * sys1;

figure(1);
bode(sys1);
figure(2);
bode(sys2);
figure(3);
bode(sys3);
