R = 100, L = 1e-3, C = 10e-9;

A = [0, 1/C; -1/L, -R/L];
B = [0; 1/L];
C = [0, -R];
D = 1;

sys = ss(A, B, C, D);

#pzmap(sys);
bode(sys);
#step(sys);
