# Calc R and C for specified Fc
# R fixed at 1k

fc = [159, 159e1, 159e2, 159e3];
R = 1000;
i = 1;

for f = fc
    C = 1/(2*pi*R*f)

    num_lp = [1];
    num_hp = [R*C, 0];
    den = [R*C, 1];
    sys_lp = tf(num_lp, den);
    sys_hp = tf(num_hp, den);
    figure(i++);
    bode(sys_lp);
    figure(i++);
    step(sys_lp);
    figure(i++);
    bode(sys_hp);
    figure(i++);
    step(sys_hp);

endfor
