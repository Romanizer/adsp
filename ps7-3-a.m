L = 10e-3;
C = 11e-9;
w0 = 1 / (sqrt(L*C));

# single
num = [w0^2];
den = [1, 0, w0^2];

syssingle = tf(num, den);
figure(1);
bode(syssingle);

systrip = syssingle*syssingle*syssingle;
figure(2);
bode(systrip);
return;
# triple
num = [w0^6];
den = [1, 0, -2*w0^2, 0, w0^4, 0, -2*w0^5+w0^4+w0^6];

systriple = tf(num, den);
figure(2);
bode(systriple)
