fs = 44100;
 % sampling period = 1 ms
t = [0:1/fs:1];
 % sample times
f0 = 440;
 % cosine frequency in Hz
x = cos(2*pi*f0*t);
x2 = sin(2*pi*f0*t);
 % DT cosine signal
#plot(t, x);
#xlabel('t [s]'), ylabel('cos(2 \pi f_0 t)');
#sound(x, fs);

Ts = 0.01;
 % sampling period
t = [0:Ts:10];
 % sample times
tau = 1.5;
 % time constant of exponential
f0 = 5;
 % sinusoidal frequency
x = exp(-t/tau) .* cos(2*pi*f0*t);
plot(t, x);
