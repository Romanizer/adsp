fs = 1000;
t = [0:1/fs:10];
f0 = 10;

x1 = sin(2*pi*f0*t).^2;
#x1 = x1.*x1;

plot(t, x1);
hold on

x2 = 0.5.*(1 + cos(2*pi*f0*t));

plot(t, x2);
hold on

chirpfn = t/10;
x3 = cos(2.*pi.*t.*chirpfn);

plot(t, x3);
