L = 10e-3;
C = 0.001e-6;

w0 = 1/sqrt(L*C);

num = [w0*w0];
den = [1, 0, w0*w0];

sys1 = tf(num, den);
bode(sys1);
